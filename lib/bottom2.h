#ifndef BOTTOM_H
#define BOTTOM_H

#ifdef _USRDLL
#define DLL_I _declspec(dllexport)
#else
#define DLL_I _declspec(dllimport)
#endif // DLL

class DLL_I Bottom2
{
public:
	Bottom2();
	~Bottom2();
	void Dump();
};

#endif

